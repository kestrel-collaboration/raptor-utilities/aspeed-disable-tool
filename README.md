## ASpeed Disable Utility

This utility can be used to non-destructively configure the ASpeed BMC on Talos II / Blackbird to release bus control and effectively halt (i.e. sit at the U-boot prompt, doing nothing).  This allows an external BMC (e.g. Kestrel) to be attached and to take complete control of the underlying host.

We use a small chunk of unused Flash memory in the BMC Flash device at address `0x1ff0000` (near the end of the Flash device) to store the configuration utility.

### Upload instructions
 * Interrupt U-boot startup immediately after power application
 * At the U-boot serial prompt run the following commands
```
loads 83000000
<paste appropriate SREC file contents>
<return>
go 83000000
erase 21ff0000 21ffffff
cp.b 83000000 21ff0000 ffff
env set bootcmd "go 21ff0000"
saveenv
```

The ASpeed BMC will now halt and allow an external BMC to control the host approximately 8 seconds after power application.

**NOTE** There is a very short time (~40s) between the U-boot prompt appearing and the BMC automatically being reset by the platform control FPGA.  The `go 83000000` command needs to have finished execution prior to this window expiring, so we recommend copying the SREC file contents to the clipboard prior to attempting this procedure.

### Removal Instructions

To revert, simply run the following at the U-boot serial prompt:
```
env set bootcmd "bootm 20080000"
saveenv
```
