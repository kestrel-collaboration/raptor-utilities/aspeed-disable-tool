/* Minimal code to (effectively) disable the ASpeed BMC and allow
 * Kestrel to take over.
 * Copyright 2019 - 2023 Raptor Engineering, LLC
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <stdint.h>

#define EINVAL 4
#define ETIMEDOUT 10
#define EREMOTEIO 11

#define HIGHSPEED_TTIMEOUT 3

#define FUNCTION_TYPE_HACK __attribute__((always_inline)) inline

#define I2C_REGISTER_DEFINITIONS	\
	volatile uint32_t* fcr_reg;	\
	volatile uint32_t* icr_reg;	\
	volatile uint32_t* isr_reg;	\
	volatile uint32_t* csr_reg;	\
	volatile uint32_t* trbbr_reg;	\
	volatile uint32_t* cactcr1_reg;	\
	volatile uint32_t* cactcr2_reg;

FUNCTION_TYPE_HACK void putc(unsigned char c)
{
  volatile uint32_t* uart5_lrbrthr_reg;
  volatile uint32_t* uart5_fcr_reg;
  volatile uint32_t* uart5_lsr_reg;

  uart5_lrbrthr_reg = (uint32_t*)(0x1e784000 + 0x00);
  uart5_fcr_reg = (uint32_t*)(0x1e784000 + 0x08);
  uart5_lsr_reg = (uint32_t*)(0x1e784000 + 0x14);

  if (*uart5_fcr_reg & (0x1 << 0)) {
    while (*uart5_lsr_reg & (0x1 << 5)) {
      // Wait for TX FIFO to drain
    }
  }
  else {
    while (!(*uart5_lsr_reg & (0x1 << 5))) {
      // Wait for TX to finish
    }
  }

  *uart5_lrbrthr_reg = c;
}

FUNCTION_TYPE_HACK void print_hex_value(uint32_t value) {
  putc('\n');
  putc('\r');
  putc('0' + ((value >> 28) & 0xf));
  putc('0' + ((value >> 24) & 0xf));
  putc('0' + ((value >> 20) & 0xf));
  putc('0' + ((value >> 16) & 0xf));
  putc('0' + ((value >> 12) & 0xf));
  putc('0' + ((value >> 8) & 0xf));
  putc('0' + ((value >> 4) & 0xf));
  putc('0' + ((value >> 0) & 0xf));
  putc('\n');
  putc('\r');
}

int main() {
  int ret;

  uint32_t dword;
  uint8_t i2c_data_buffer[2];

  volatile uint32_t* scu00_reg;
  volatile uint32_t* scu04_reg;
  volatile uint32_t* scu70_reg;
  volatile uint32_t* scu88_reg;
  volatile uint32_t* scu8c_reg;
  volatile uint32_t* scu94_reg;
  volatile uint32_t* scua4_reg;
  volatile uint32_t* scua8_reg;
  volatile uint32_t* scuac_reg;
  volatile uint32_t* gfx64_reg;
  volatile uint32_t* i2c_gpio_ctl_reg;
  volatile uint32_t* gpio_abcd_ctl_reg;
  volatile uint32_t* gpio_abcd_data_reg;
  volatile uint32_t* gpio_efgh_ctl_reg;
  volatile uint32_t* gpio_efgh_data_reg;
  volatile uint32_t* gpio_mnop_ctl_reg;
  volatile uint32_t* gpio_mnop_data_reg;
  volatile uint32_t* gpio_qrst_ctl_reg;
  volatile uint32_t* gpio_qrst_data_reg;

  scu00_reg = (uint32_t*)0x1e6e2000;
  scu04_reg = (uint32_t*)0x1e6e2004;
  scu70_reg = (uint32_t*)0x1e6e2070;
  scu88_reg = (uint32_t*)0x1e6e2088;
  scu8c_reg = (uint32_t*)0x1e6e208c;
  scu94_reg = (uint32_t*)0x1e6e2094;
  scua4_reg = (uint32_t*)0x1e6e20a4;
  scua8_reg = (uint32_t*)0x1e6e20a8;
  scuac_reg = (uint32_t*)0x1e6e20ac;
  gfx64_reg = (uint32_t*)0x1e6e6064;
  i2c_gpio_ctl_reg = (uint32_t*)0x1e6e2090;
  gpio_abcd_ctl_reg = (uint32_t*)0x1e780004;
  gpio_abcd_data_reg = (uint32_t*)0x1e780000;
  gpio_efgh_ctl_reg = (uint32_t*)0x1e780024;
  gpio_efgh_data_reg = (uint32_t*)0x1e780020;
  gpio_mnop_ctl_reg = (uint32_t*)0x1e78007c;
  gpio_mnop_data_reg = (uint32_t*)0x1e780078;
  gpio_qrst_ctl_reg = (uint32_t*)0x1e780084;
  gpio_qrst_data_reg = (uint32_t*)0x1e780080;

  putc('A');

  // Disable BMC boot watchdog
  *gpio_efgh_ctl_reg |= 0x00010000;
  *gpio_efgh_data_reg &= ~0x00010000;

  // Enable SCU access
  *scu00_reg = 0x1688a8a8;
  if (*scu00_reg != 0x1) {
    return -1;
  }

  // Take CRT controller out of reset
  *scu04_reg &= ~0x00002000;

  // Enable the onboard VGA controller
  *scu70_reg |= 0x00008000;

  // Enable DVO
  *scu8c_reg |= 0x000000ff;
  *scua4_reg |= 0xffff0000;
  *scua8_reg |= 0x0000000f;
  *scu94_reg = (*scu94_reg & ~0x00000003) | 0x00000001;
  *gfx64_reg |= 0x00000080;

  // Disable AST LPC pins
  *scuac_reg &= ~0x000000ff;

  // Switch FSI GPIOs (H1,H2,H3) to input mode
  *gpio_efgh_ctl_reg &= ~0x0000000e;

  // Disable AST I2C bus 13 pins
  *i2c_gpio_ctl_reg &= ~0x04000000;

  // Disable AST I2C bus 6 pins (HDMI transceiver setup / control)
  *i2c_gpio_ctl_reg &= ~0x00080000;

  // Enable GPIO R1
  *scu88_reg &= ~0x02000000;
  if (*scu88_reg & 0x02000000) {
    return -2;
  }

  // Take I2C controllers out of reset
  *scu04_reg &= ~0x00000004;

  // Disable SCU access
  *scu00_reg = 0x00000000;

  putc('B');

  // Deactivate PM overheat signal (pull GPIO B5 high)
  // Note that for an unknown reason, the GPIO
  // register needs a separate read / write
  // cycle to actually take effect...
  *gpio_abcd_ctl_reg |= 0x00002000;
  dword = *gpio_abcd_data_reg;
  dword |= 0x00002000;
  *gpio_abcd_data_reg = dword;

  // Deactivate UID request (pull GPIO N4 low)
  // Deactivate PM fan fail signal (pull GPIO N2 high)
  // Note that for an unknown reason, the GPIO
  // register needs a separate read / write
  // cycle to actually take effect...
  *gpio_mnop_ctl_reg |= 0x00001400;
  dword = *gpio_mnop_data_reg;
  dword &= ~0x00001000;
  dword |= 0x00000400;
  *gpio_mnop_data_reg = dword;

  // Signal platform start ready (GPIO R1 low)
  // Shut off inboard power LED (GPIO R5 high)
  // Note that for an unknown reason, the GPIO
  // register needs a separate read / write
  // cycle to actually take effect...
  *gpio_qrst_ctl_reg |= 0x00002200;
  dword = *gpio_qrst_data_reg;
  dword &= ~0x00000200;
  dword |= 0x00002000;
  *gpio_qrst_data_reg = dword;

  // Success!
  return 0;
}
