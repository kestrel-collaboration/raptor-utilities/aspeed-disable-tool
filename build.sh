#!/bin/bash

set -e

rm -f blackbird.srec
rm -f talos_ii.srec

arm-linux-gnueabihf-gcc -ffreestanding -march=armv6 -mfloat-abi=soft -marm -Os -g -c blackbird.c
arm-linux-gnueabihf-gcc -ffreestanding -march=armv6 -mfloat-abi=soft -marm -Os -g -c talos_ii.c

arm-linux-gnueabihf-objcopy -O srec blackbird.o blackbird.srec
arm-linux-gnueabihf-objcopy -O srec talos_ii.o talos_ii.srec

arm-linux-gnueabihf-objdump -S blackbird.o &> blackbird.S
arm-linux-gnueabihf-objdump -S talos_ii.o &> talos_ii.S
