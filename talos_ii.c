/* Minimal code to (effectively) disable the ASpeed BMC and allow
 * Kestrel to take over.
 * Copyright 2019 - 2021 Raptor Engineering, LLC
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <stdint.h>

int main() {
  uint32_t dword;

  volatile uint32_t* scu00_reg = (uint32_t*)0x1e6e2000;
  volatile uint32_t* scu88_reg = (uint32_t*)0x1e6e2088;
  volatile uint32_t* scuac_reg = (uint32_t*)0x1e6e20ac;
  volatile uint32_t* i2c_gpio_ctl_reg = (uint32_t*)0x1e6e2090;
  volatile uint32_t* gpio_qrst_ctl_reg = (uint32_t*)0x1e780084;
  volatile uint32_t* gpio_qrst_data_reg = (uint32_t*)0x1e780080;
  volatile uint32_t* gpio_yzaaab_ctl_reg = (uint32_t*)0x1e7801e4;
  volatile uint32_t* gpio_yzaaab_data_reg = (uint32_t*)0x1e7801e0;

  // Disable BMC boot watchdog
  *gpio_qrst_ctl_reg |= 0x00800000;
  *gpio_qrst_data_reg &= ~0x00800000;

  // Enable SCU access
  *scu00_reg = 0x1688a8a8;
  if (*scu00_reg != 0x1) {
    return -1;
  }

  // Disable AST LPC pins
  *scuac_reg &= ~0x000000ff;

  // Switch FSI GPIOs (AA0,AA2,R2) to input mode
  *gpio_yzaaab_ctl_reg &= ~0x00000005;
  *gpio_qrst_data_reg &= ~0x00000004;

  // Disable AST I2C bus 13 pins
  *i2c_gpio_ctl_reg &= ~0x04000000;

  // Enable GPIO R1
  *scu88_reg &= ~0x02000000;
  if (*scu88_reg & 0x02000000) {
    return -2;
  }

  // Disable SCU access
  *scu00_reg = 0x00000000;

  // Signal platform start ready (GPIO R1 low)
  // Note that for an unknown reason, the GPIO
  // register needs a separate read / write
  // cycle to actually take effect...
  *gpio_qrst_ctl_reg |= 0x00000200;
  dword = *gpio_qrst_data_reg;
  dword &= ~0x00000200;
  *gpio_qrst_data_reg = dword;

  // Success!
  return 0;
}
